// game code

// variables
var run = false;

var canvas = document.getElementById("game_canvas");
var context = canvas.getContext("2d");

var wordScale = 1.0;

var letter_index = 0;
var word_index = -1;
var text = "SLOW";

var words = [
	"SHIT",
	"FART"];

// pop next word
function next_word()
{
	letter_index = 0;
	word_index++;
	if (word_index == words.length) word_index = 0;

	text = words[word_index];

	say();

	wordScale = 1.5;
}

// say the word
function say()
{
	window.speechSynthesis.cancel();
	window.speechSynthesis.speak(new SpeechSynthesisUtterance(text));
}

// init renderer
function init()
{
	canvas.style.width = window.innerWidth + "px";
	canvas.style.height = window.innerHeight + "px";
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;
}

// render frame
function frame()
{
	context.clearRect(0, 0, canvas.width, canvas.height);

	var fontHeight = canvas.height / 5 * wordScale;
	context.font = fontHeight + "px Arial";
	
	var txtWidth = context.measureText(text).width;
	var txtOffsetWidth = context.measureText(text.substring(0, letter_index)).width;

	context.fillStyle = "#FF0000";
	context.fillText(text.substring(0, letter_index), (canvas.width - txtWidth) / 2, (canvas.height + fontHeight / 2) / 2);
	context.fillStyle = "#000000";
	context.fillText(text.substring(letter_index), (canvas.width - txtWidth) / 2 + txtOffsetWidth, (canvas.height + fontHeight / 2) / 2);

	wordScale = (1.0 + wordScale) / 2;
}

function title()
{
	context.clearRect(0, 0, canvas.width, canvas.height);

	var fontHeight = canvas.height / 10 * wordScale;
	context.font = fontHeight + "px Arial";

	var titleText = "PRESS ANY LETTER TO START";
	
	var txtWidth = context.measureText(titleText).width;

	context.fillStyle = "#000000";
	context.fillText(titleText, (canvas.width - txtWidth) / 2, (canvas.height + fontHeight / 2) / 2);	
}

// key down
function keydown(e)
{
	switch(e.key.toUpperCase())
	{
		case "ESCAPE":
			run = false;
			break;
		case "A": case "B": case "C": case "D": case "E": case "F": case "G": case "H": case "I":
		case "J": case "K": case "L": case "M": case "N": case "O": case "P": case "Q": case "R":
		case "S": case "T": case "U": case "V": case "W": case "X": case "Y": case "Z":
			if (!run)
			{
				run = true;				
				next_word();
			}
			else
			{
				letter_index++;

				if (e.key.toUpperCase().charAt(0) != text.toUpperCase().charAt(letter_index - 1)) 
				{
					letter_index = 0;
					say();
				}

				if (letter_index == text.length) next_word();
			}
			break;
	}
}

// game tick
function tick()
{
	if (run) frame();
	else title();
}

window.onresize = init;
document.addEventListener("keydown", keydown, false);

init();

setInterval(tick, 15);
